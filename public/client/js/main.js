function fullpageSlideMarina() {
	const sm = $(window).width();
	if (sm >= 991) {
		$('#fullpageMarina').fullpage({
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: [
				'Tuần Châu Marina',
				'Chính sách ưu đãi',
				'Vị thế đắc địa',
				'Chủ đầu tư dự án',
				'Thông tin dự án',
				'Quy hoạch',
				'Tiện ích bên ngoài',
				'Tiện ích bên trong',
				'Video giới thiệu',
				'Đăng ký đầu tư'
			],
			css3: true
		});
	}
}
$(document).ready(function() {
	fullpageSlideMarina();
	$('.carousel').carousel();
});
