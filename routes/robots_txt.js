const express = require('express');
const router = express.Router();

const fs = require('fs');

router.get('/', function (req, res, next) {
    fs.readFile(process.cwd() + '/robots.txt', 'utf8', (err, content) => {
        if (err) return next(err);

        res.type('text/plain');
        res.send(content);
    });
});

module.exports = router;
