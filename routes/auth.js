const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/', (req, res) => {
	if (req.user) return res.redirect('/admin/dashboard');
	res.render('admin/main/signIn');
});

router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/auth');
});

router.post('/login', passport.authenticate('local', {
	successRedirect: '/admin/dashboard',
	failureRedirect: '/admin/auth',
	failureFlash: true
}));

module.exports = router;
