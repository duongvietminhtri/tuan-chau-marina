const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const checkEmail = '/^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/';
const checkNumber0 = /^09[0-46-9][0-9]{7}$|^012[0-9]{8}$|^016[2-9][0-9]{7}$|^018[68][0-9]{7}$|^0199[0-9]{7}$|^08[689][0-9]{7}$|^02[0-9]{9}$/;
const checkNumber84 = /^\+849[0-46-9][0-9]{7}$|^\+8412[0-9]{8}$|^\+8416[2-9][0-9]{7}$|^\+8418[68][0-9]{7}$|^\+84199[0-9]{7}$|^\+848[689][0-9]{7}$|^\+842[0-9]{9}$/;

const RegisterSchema = new Schema({
	fullName: {
		type: String,
		required: true,
		trim: true
	},
	phone: {
		type: String,
		required: true,
		trim: true,
	},
	email: {
		type: String,
		lowercase: true,
		trim: true,
	},
	content: {
		type: String,
		trim: true
	},
	isHandled: { type: Boolean, default: false },
	deleteAt: { type: Date }
}, {
	timestamps: true
});

module.exports = mongoose.model('Register', RegisterSchema);
