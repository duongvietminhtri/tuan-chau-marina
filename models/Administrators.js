const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const AdministratorsSchema = new Schema({
	email: {
		type: String,
		unique: true,
		lowercase: true,
		required: true,
		trim: true,
	},
	password: { type: String, required: true },
	profile: {
		name: {
			type: String,
			trim: true,
			default: 'Anonymous'
		},
		picture: String
	},
	resetPasswordToken: String,
	resetPasswordExpires: Date,
	isBlocked: { type: Boolean, default: false }
}, {
	timestamps: true
});

AdministratorsSchema.pre('save', function (next) {
	const user = this;
	if (!user.isModified('password')) return next();
	bcrypt.genSalt(10, function(err, salt) {
		if (err) return next(err);
		bcrypt.hash(user.password, salt, function(err, hash) {
			if (err) return next(err);
			user.password = hash;
			next();
		});
	});
});

AdministratorsSchema.methods.comaprePassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};

AdministratorsSchema.methods.gravatar = (size) => {
	if (!this.size) size = 150;
	if (!this.email) return `https://gravatar.com/avatar/?s=${size}&d=retro`;
	const md5 = crypto.createHash('md5').update(this.email).digest('hex');
	return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
};

module.exports = mongoose.model('Administrator', AdministratorsSchema);
